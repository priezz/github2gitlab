Tool to migrate `git` repositories from GitHub to GitLab. Transferes issues, milestones, labels and wiki. 
It is possible to overwrite target project if required. Non existing target users are created on demand.

**Note:** Authoring of issues/comments is not preserved.

# Installation
**Note:** [Bundler](http://bundler.io/) is required.

```
git clone https://gitlab.com/priezz/github2gitlab.git
cd github2gitlab
bundle install
```


# Usage

```
ruby github2gitlab.rb <arguments>
```

```
Usage: github2gitlab [options]
    -u, --user USER                  user to connect to GitHub with
    -p, --pw PASSWORD                password for user to connect to GitHub with
        --api API                    API endpoint for GitHub
        --gitlab-api API             API endpoint for GitLab
    -t, --gitlab-token TOKEN         Private token for GitLab
    -c, --create-users               Create unexisting users (requires GitLab server administrative rights)
        --web                        Web endpoint for GitHub
        --ssh                        Use ssh for GitHub
        --private                    Import only private GitHub repositories (enables ssh)
    -s, --space SPACE                The space to import repositories from (User or Organization)
    -r, --repo REPO                  The GitHub repository within SPACE to import from. If omitted, all repos from SPACE will be imported
    -g, --group GROUP                The GitLab group to import projects to
    -f, --force                      Force the repo transfer, overwrites destination GitLab project is exists
    -h, --help                       Display this screen
```

# Links to original sources

https://gist.github.com/tonyarnold/9733445<br/>
https://gitlab.com/gitlab-org/gitlab-recipes/blob/master/import/github/import_all.rb
