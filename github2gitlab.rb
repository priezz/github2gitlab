# This very script is a deep modification by Alexandr PRIEZZHEV (2016) of two earlier community scripts:
# - https://gist.github.com/tonyarnold/9733445
# - https://gitlab.com/gitlab-org/gitlab-recipes/blob/master/import/github/import_all.rb
#
# It imports labels, milestones, repositories, issues, comments and the wikis.
# Please feel free to send merge requests to improve it or just file bugs discovered.
# 

require 'bundler/setup'
require 'octokit'
require 'optparse'
require 'git'
require 'gitlab'
require 'awesome_print'

tmp_dir = "/tmp/github2gitlab"


## Deal with options from cli, like username and pw
options = {:usr => nil,
           :pw => nil,
           :api => 'https://api.github.com',
           :web => 'https://github.com/',
           :space => nil,
           :group => nil,
           :repo => nil,
           :ssh => false,
           :create_users => false,
           :private => false,
           :force => false,
           :gitlab_api => 'http://gitlab.example.com/api/v3',
           :gitlab_token => 'secret'
           }
optparse = OptionParser.new do |opts|
  opts.on('-u', '--user USER', "user to connect to GitHub with") do |u|
    options[:usr] = u
  end
  opts.on('-p', '--pw PASSWORD', 'password for user to connect to GitHub with') do |p|
    options[:pw] = p
  end
  opts.on('--api API', String, 'API endpoint for GitHub') do |a|
    options[:api] = a
  end
  opts.on('--gitlab-api API', String, 'API endpoint for GitLab') do |a|
    options[:gitlab_api] = a
  end
  opts.on('-t', '--gitlab-token TOKEN', String, 'Private token for GitLab') do |t|
    options[:gitlab_token] = t
  end
  opts.on('-c', '--create-users', 'Create unexisting users (requires GitLab server administrative rights)') do |c|
  	options[:create_users] = c
  end
  opts.on('--web', 'Web endpoint for GitHub') do |w|
    options[:web] = w
  end
  opts.on('--ssh', 'Use ssh for GitHub') do |s|
    options[:ssh] = s
  end
  opts.on('--private', 'Import only private GitHub repositories (enables ssh)') do |p|
    options[:private] = p
    options[:ssh] = true
  end
  opts.on('-s', '--space SPACE', 'The space to import repositories from (User or Organization)') do |s|
    options[:space] = s
  end
  opts.on('-r', '--repo REPO', 'The GitHub repository within SPACE to import from') do |r|
    options[:repo] = r
  end
  opts.on('-g', '--group GROUP', 'The GitLab group to import projects to') do |g|
    options[:group] = g
  end
  opts.on('-f', '--force', 'Force the repo transfer, overwrites destination GitLab project is exists') do |f|
  	options[:force] = f
  end
  opts.on('-h', '--help', 'Display this screen') do
    puts opts
    exit
  end
end

optparse.parse!
if options[:usr].nil? or options[:pw].nil?
  puts "Missing parameter ..."
  puts options
  exit
end

if options[:group].nil?
  if options[:space].nil?
    raise 'Both group and space can\'t be empty!'
  end
  options[:group] = options[:space]
end



def random_string
  o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
  (0...50).map { o[rand(o.length)] }.join
end

$users_hash = {}

def find_or_create_user(username, github_client, gitlab_client, create_users)
  user_hashed = $users_hash.key?(username)
  if user_hashed
    #puts "User %s found" % username
  	found_user = $users_hash[username]
  else
    #puts "User %s not found, trying to find..." % username
  	found_user = nil
  end

  # Look for an existing user
  if not user_hashed and found_user.nil?
    found_users = gitlab_client.users( { "username" => username } )
    if found_users.any?
  	  found_user = found_users[0]
    end
  end

  # If an existing user can't be found, create a new one
  if not user_hashed and found_user.nil? and create_users == true
    gh_user = github_client.user(username)
    found_user = gitlab_client.create_user( "gitlab-#{gh_user[:login]}@example.com", random_string, "#{gh_user[:login]}", :name => gh_user[:name] )
  end

  # Save user into local hash-table
  if not user_hashed
  	ap found_user
  	$users_hash[username] = found_user
  end
  found_user
end

def find_or_create_milestone(project_id, milestone_title, description, due_on, github_client, gitlab_client)
  found_milestone = nil
  
  # Look for an existing milestone
  gitlab_client.milestones(project_id).each do |m|
    if m.title == milestone_title && m.project == project_id
      found_milestone = m
    end
  end
  
  # If an existing milestone can't be found, create a new one
  if found_milestone.nil?    
    found_milestone = gitlab_client.create_milestone(project_id, milestone_title, {
      :description => description,
      :due_date => due_on
    })
  end
    
  found_milestone
end




Octokit.configure do |c|
  c.api_endpoint = options[:api]
  c.web_endpoint = options[:web]
end

# Set the GitLab options
Gitlab.configure do |c|
  c.endpoint = options[:gitlab_api]
  c.private_token = options[:gitlab_token]
end

#setup the clients
gh_client = Octokit::Client.new(:login => options[:usr], :password => options[:pw])
gl_client = Gitlab.client( endpoint: options[:gitlab_api], private_token: options[:gitlab_token] )

# Find or create the target GitLab group
push_group = nil
# I should be able to search for a group by name
gl_client.groups.each do |g|
  if g.name == options[:group]
    push_group = g
  end
end

#if the group wasn't found, create it
user = gl_client.user()
if push_group.nil?
  if options[:group] == user.username
    push_group = OpenStruct.new
    push_group.web_url = user.web_url.gsub( "/u/", "/" )
  else
    push_group = gl_client.create_group(options[:group], options[:group])
  end
end

#get all of the repos that are in the specified space (user or org)
gh_repos = gh_client.repositories(options[:space], {:type => options[:private] ? 'private' : 'all'})
gh_repos.each do |gh_r|
  next if options[:repo] != nil and gh_r.name != options[:repo]

  name = gh_r.name.gsub( ".", "-" )
  # Edge case, gitlab didn't like names that didn't start with an alpha. Can't remember how I ran into this.
  if gh_r.name !~ /^[a-zA-Z]/
	name = "gh-#{gh_r.name}"
  end

  expected_web_url = push_group.web_url.gsub( "/groups/", "/" ) + "/" + name
  project = nil
  #I should be able to search for a group by name
  gl_client.projects.each do |p|
    if expected_web_url == p.web_url
      project = p
    end
  end
  # If the project found, skip it
  unless project.nil?
    if options[:force] == true
   	  puts "Overwriting destination project..."
      gl_client.delete_project( project.id )
	  puts "Waiting 120 seconds for repository deletion on the GitLab server..."
	  sleep(120)
    else
      puts "Project '%s' already exists at path '%s', skipping..." % [ name, expected_web_url ]
      next
    end
  end

  puts "Processing project '%s' (will be available at '%s')..." % [ name, expected_web_url ]

  ## Create the project on GitLab 
  ## - http://www.rubydoc.info/gems/gitlab/Gitlab/Client/Projects#create_project-instance_method
  if options[:group] == user.username
    new_project = gl_client.create_project( name, {
      default_branch: gh_r.default_branch,
      description: gh_r.description,
      public: not(gh_r.private),
      wiki_enabled: gh_r.has_wiki
  } )
  else
    new_project = gl_client.create_project( name, {
      namespace_id: push_group.id,
      group_id: push_group.id,
      default_branch: gh_r.default_branch,
      description: gh_r.description,
      public: not(gh_r.private),
      wiki_enabled: gh_r.has_wiki
    } )
  end

  ## Clone the repo from the GitHub server
  `rm -rf "#{tmp_dir}/#{gh_r.name}"; mkdir -p "#{tmp_dir}"`
  git_repo = Git.clone(options[:ssh] ? gh_r.ssh_url : gh_r.git_url, gh_r.name, :path => tmp_dir )
  `for branch in $(git --git-dir #{tmp_dir}/#{gh_r.name}/.git branch -a | grep remotes | grep -v HEAD | grep -v master); do git --git-dir #{tmp_dir}/#{gh_r.name}/.git branch --track ${branch##*/} $branch;  done`

  ## Push the project to GitLab 
  git_repo.add_remote("gitlab", options[:ssh] ? new_project.ssh_url_to_repo : new_project.http_url_to_repo)
  git_repo.push('gitlab', '--all')
  
  ## Look for milestones in GitHub for this project and push them to GitLab
  milestone_hash = {}
  puts "Copying #{gh_r.name} milestones: "

  milestones = gh_client.milestones(gh_r.full_name)
  milestones.each do |m|
    gl_milestone = find_or_create_milestone(new_project.id, m.title, m.description, m.due_on, gh_client, gl_client)  
    milestone_hash[gl_milestone.title] = gl_milestone.id
    puts "  ✓ Copied milestone #{gl_milestone.title} (#{gl_milestone.id})"
  end

  ## Copy labels for this project
  puts "Copying #{gh_r.name} labels... "
  labels = gh_client.labels(gh_r.full_name)
  labels.each do |l|
    gl_client.create_label( new_project.id, l.name, '#'+l.color )
  end

  ## Look for issues in GitHub for this project and push them to GitLab
  #
  if gh_r.has_issues
    puts "Copying issues for #{gh_r.name}: "

	issues = []
    
	puts "Processing open issues..."
	# Get opened issues
	page = 1
	loop do
	  issues_ = gh_client.list_issues(gh_r.full_name, :page => page)
	  issues.concat(issues_)
	  page = page + 1
	  break if issues_.size() < 30 # Github returns 30 issues per page
	end
    
	puts "Processiong closed issues..."
	# Get closed issues
	page = 1
	loop do
	  issues_ = gh_client.list_issues(gh_r.full_name, :page => page, :state => 'closed')
	  issues.concat(issues_)
	  page = page + 1
	  break if issues_.size() < 30
	end
     
    ## Sort issues by number
	issues.sort_by! { |i| i.number }
    
    issues.each do |i|
      issue_options = {
        :description => i.body,
        :created_at => i.created_at
      }
 
      ## GitLab 8.6.x API does not support setting the author
      #unless i.user.nil?
        #issue_author = find_or_create_user( i.user[:login], gh_client, gl_client, options[:create_users] )
        #unless issue_author.nil?
          #issue_options[:author_id] = issue_author.id
        #end
      #end

      unless i.assignee.nil?
        issue_assignee = find_or_create_user( i.assignee[:login], gh_client, gl_client, options[:create_users] )
        unless issue_assignee.nil?
          issue_options[:assignee_id] = issue_assignee.id
        end
      end

      unless i.milestone.nil? or i.milestone.title.nil? or milestone_hash[i.milestone.title].nil?      
        issue_options[:milestone_id] = milestone_hash[i.milestone.title]
      end
      
      labels_for_issue = i.labels.map {|l| l.name }.join(',')
      
      unless labels_for_issue.empty?
        issue_options[:labels] = labels_for_issue
      end
      
      gl_issue = gl_client.create_issue(new_project.id, i.title, issue_options)
      
      if i.state.eql? "closed"
        gl_client.close_issue(new_project.id, gl_issue.id)
      end
      
      puts "  ✓ Copied issue ##{i.number} - #{gl_issue.title} by #{i.user[:login]} with state #{i.state}"
      
      comments = gh_client.issue_comments(gh_r.full_name, i.number)
      if comments.any?
      	counter = 0
        comments.each do |c|
          counter = counter + 1
          comment_body = "#{c.body}\n\n\n*Comment originally by [#{c.user.login}](#{c.user.html_url}) on #{c.created_at}*"
          gl_issue_note = gl_client.create_issue_note(new_project.id, gl_issue.id, comment_body)
          puts "    ✓ Copied comment ##{counter} for issue ##{i.number}"
        end
      end
    end
  end
  
  ## Look for wiki pages for this repo in GitHub and migrate them to GitLab
  if gh_r.has_wiki
    puts "Copying #{gh_r.name} wiki: "

    # This is dumb. The only way to know if a repo has a wiki is to attempt to clone it and then ignore failure if it doesn't have one
    begin
      gh_wiki_url = gh_r.git_url.gsub(/\.git/, ".wiki.git")
      wiki_name = gh_r.name + '.wiki'
      wiki_repo = Git.clone(gh_wiki_url, wiki_name, :path => '/tmp/clones')

      # This is a pain, have to visit the wiki page on the web ui before being able to work with it as a git repo
      wget_dir = tmp_dir + "/$"
      `rm -rf "#{wget_dir}"; mkdir -p "#{wget_dir}"`
      `wget -q --save-cookies #{wget_dir}/gl_login.txt -P #{wget_dir} --post-data "username=#{options[:usr]}&password=#{options[:pw]}" #{options[:gitlab_api]}/users/auth/ldap/callback`
      `wget -q --load-cookies #{wget_dir}/gl_login.txt -P #{wget_dir} -p #{new_project.web_url}/wikis/home`
      `rm -rf #{wget_dir}`

      gl_wiki_url = (options[:ssh] ? new_project.ssh_url_to_repo : new_project.http_url_to_repo).gsub(/\.git/, ".wiki.git")
      wiki_repo.add_remote('gitlab', gl_wiki_url)
      wiki_repo.push('gitlab')
    rescue
    end
  end
end

